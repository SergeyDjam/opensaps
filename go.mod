module go.dev.pztrn.name/opensaps

go 1.13

require (
	github.com/pztrn/mogrus v0.0.0-20180323033502-2d9bba232129
	github.com/sirupsen/logrus v1.4.2 // indirect
	go.dev.pztrn.name/flagger v0.0.0-20191215171500-5e6aeb0e0620
	gopkg.in/yaml.v2 v2.2.7
)
